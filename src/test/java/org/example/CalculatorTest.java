package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void add() {
        assertEquals(3,Calculator.add(1,2));
    }

    @Test
    void minus() {
        assertEquals(3,Calculator.minus(5,2));
    }

    @Test
    void multiply() {
        assertEquals(4,Calculator.multiply(2,2));
    }

    @Test
    void divide() {
        assertEquals(3,Calculator.divide(6,2));
    }
    @Test
    void percentagem(){
        assertEquals(1,Calculator.percentagem(9,2));
    }
}